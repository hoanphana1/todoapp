package com.hoanphan.tasktodo.model;

import java.io.Serializable;

import io.realm.RealmObject;

public class Note extends RealmObject implements Serializable {

    private int id;
    private String title;
    private String decription;
    private String createDate;

    public Note() {
    }

    public Note(String title, String decription, String createDate) {
        this.title = title;
        this.decription = decription;
        this.createDate = createDate;
    }
    public void copyFrom(Note newNote){
        this.setId(newNote.getId());
        this.setTitle(newNote.getTitle());
        this.setDecription(newNote.getDecription());
        this.setCreateDate(newNote.getCreateDate());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
