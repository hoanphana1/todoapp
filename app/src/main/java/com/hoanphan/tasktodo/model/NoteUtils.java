package com.hoanphan.tasktodo.model;

import java.util.ArrayList;

public class NoteUtils {
    //singleton
    private ArrayList<Note> noteData;
    private static NoteUtils noteUtils;

    private NoteUtils() {   //1 private contructor
        noteData = new ArrayList<>();
    }
    public static NoteUtils getInstance(){
        if(noteUtils == null){
            noteUtils = new NoteUtils();
        }
        return noteUtils;
    }//singleton



    public ArrayList<Note> getAllNotes(){
        return noteData;
    }
    public void addNote(Note note){
       if(noteData.isEmpty()){
           note.setId(0);
       }else {
           int lastNoteID = noteData.get(noteData.size() - 1).getId();
           note.setId(lastNoteID + 1);
       }
       noteData.add(note);
    }
    public void updateNote(int id,Note newNote){
        Note oldNote = getNoteById(id);
        if(oldNote != null){
            oldNote.setTitle(newNote.getTitle());
            oldNote.setDecription(newNote.getDecription());
        }

    }
    private Note getNoteById(int id){
        for(Note note : noteData){
            if(note.getId()==id) return note;
        }
        return null;
    }
    public void deleteNote(int id){
        for (int i = 0; i<noteData.size();i++){
            if(noteData.get(i).getId() == id){
                noteData.remove(i);
                return;
            }
        }

    }
}
