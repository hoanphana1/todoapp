package com.hoanphan.tasktodo.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hoanphan.tasktodo.R;
import com.hoanphan.tasktodo.dbContext.RealmContext;
import com.hoanphan.tasktodo.model.Note;
import com.hoanphan.tasktodo.model.NoteUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UpdateActivity extends AppCompatActivity {
    EditText edtTitle;
    EditText edtDescription;
    Button btnSave;
    Button btnCancel;
    private Note note;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_update);

        init();
        addListener();
    }
    private void init(){
        edtTitle = findViewById(R.id.edt_tittle);
        edtDescription = findViewById(R.id.edt_decription);
        btnSave = findViewById(R.id.btn_save);
        btnCancel = findViewById(R.id.btn_cancel);

        note = (Note) getIntent().getSerializableExtra("Note data");
        edtTitle.setText(note.getTitle());
        edtDescription.setText(note.getDecription());

    }
    private void addListener(){
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = edtTitle.getText().toString();
                String decription = edtDescription.getText().toString();
                if(title.isEmpty()){
                    showToast("Bạn chưa nhập tiêu đề");
                    return;
                }
                if(decription.isEmpty()){
                    showToast("Bạn chưa nhập mô tả");
                    return;
                }

                note.setTitle(title);
                note.setDecription(decription);

                RealmContext.getInstance().updateNote(note.getId(),note);
                showToast("Chỉnh sửa thành công");
                finish();
            }
        });
    }
    private void showToast(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
