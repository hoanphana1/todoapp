package com.hoanphan.tasktodo.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.hoanphan.tasktodo.R;
import com.hoanphan.tasktodo.adapter.NoteAdapter;
import com.hoanphan.tasktodo.dbContext.RealmContext;
import com.hoanphan.tasktodo.interf.OnNoteItemClickListener;
import com.hoanphan.tasktodo.model.Note;
import com.hoanphan.tasktodo.model.NoteUtils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnNoteItemClickListener{
    Button btn_add;
    RecyclerView recyclerView;
    NoteAdapter noteAdapter;
    List<Note> noteData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        addListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        noteAdapter.updateData(RealmContext.getInstance().getAll());

    }

    private void init(){
        recyclerView = findViewById(R.id.rv_note);
        btn_add = findViewById(R.id.btn_add);

        noteData = RealmContext.getInstance().getAll();//get note trong data realm ra

        noteAdapter = new NoteAdapter(noteData,this);//this -> onItemClickListener
        recyclerView.setAdapter(noteAdapter);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

    }
    private void addListener(){
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddActivity();
            }
        });
    }
    private void goToAddActivity(){
        Intent intent = new Intent(this,AddActivity.class);
        startActivity(intent);
    }
    private void goToUpdateActivity(Note note){
        Intent intent = new Intent(this,UpdateActivity.class);
        intent.putExtra("Note data",note);
        startActivity(intent);
    }

    @Override
    public void onItemClick(Note note) {
        goToUpdateActivity(note);
    }

    @Override
    public void onDeleteClick(final int id) {
        new AlertDialog.Builder(this)
                .setTitle("Xác nhận xóa")
                .setMessage("Bạn có chắc muốn xóa không?")
                .setIcon(R.drawable.alert)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        //NoteUtils.getInstance().deleteNote(id);
                        RealmContext.getInstance().DeleteNoteAt(id);
                        noteAdapter.updateData(RealmContext.getInstance().getAll());
                    }
                })
                .setNegativeButton("không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {

                    }
                })
                .show();
    }
}