package com.hoanphan.tasktodo.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hoanphan.tasktodo.R;
import com.hoanphan.tasktodo.dbContext.RealmContext;
import com.hoanphan.tasktodo.interf.OnNoteItemClickListener;
import com.hoanphan.tasktodo.model.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {

   private List<Note> noteData ;
   private OnNoteItemClickListener listener;

    public NoteAdapter(List<Note> noteData,OnNoteItemClickListener listener) {
        this.noteData = noteData;
        this.listener = listener;
    }
    public void updateData(List<Note> notes){
        noteData.clear();
        noteData.addAll(notes);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_note, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(noteData.get(position));
    }

    @Override
    public int getItemCount() {
        return noteData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvTitle;
        TextView tvCreateDate;
        ImageView imgDelete;

        private Note note;

        public ViewHolder(@NonNull View itemView){
            super(itemView);

            init(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(note);
                }
            });
            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onDeleteClick(note.getId());
                }
            });
        }

        private void init(View itemView) {
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvCreateDate = itemView.findViewById(R.id.tv_CreateDate);
            imgDelete = itemView.findViewById(R.id.img_Del);
        }

        private void bindView(Note note){
            tvTitle.setText(note.getTitle());
            tvCreateDate.setText(note.getCreateDate());
            this.note = note;
        }
    }
}
