package com.hoanphan.tasktodo.interf;

import com.hoanphan.tasktodo.model.Note;

public interface OnNoteItemClickListener {
    void onItemClick(Note note);
    void onDeleteClick(int id);
}