package com.hoanphan.tasktodo.dbContext;

import android.app.NotificationManager;
import android.content.Context;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hoanphan.tasktodo.model.Note;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmContext  {
    private Realm realm;
    private static RealmContext realmContext;//SingleTon

    private RealmContext(){
        realm = Realm.getDefaultInstance();
    }

    public static RealmContext getInstance(){
        if(realmContext == null) realmContext = new RealmContext();
        return realmContext;
    }
    public List<Note> getAll(){
        return realm.copyFromRealm(realm.where(Note.class).findAll());
    }

    public void addNote(final Note note){
        final List<Note> noteList = getAll();
        if (noteList == null || noteList.isEmpty()) {
            note.setId(0);
        }
        else {
            int lastNoteId = noteList.get(noteList.size() - 1).getId();
            note.setId(lastNoteId + 1);
            }
        realm.beginTransaction();
        Note newNote = realm.createObject(Note.class);
        newNote.copyFrom(note);
        realm.copyFromRealm(newNote);
        realm.commitTransaction();
        }

    public void updateNote(int oldId, Note newNote){
        Note oldNote = getNoteById(oldId);

        realm.beginTransaction();
        oldNote.copyFrom(newNote);
        realm.commitTransaction();
    }
    public Note getNoteById(int nodeId){
        return realm.where(Note.class).equalTo("id",nodeId).findFirst();
    }

    public void DeleteNoteAt(int noteId) {
        Note note = getNoteById(noteId);

        realm.beginTransaction();
        note.deleteFromRealm();
        realm.commitTransaction();

    }

    public void DeleteAllNote(final Context context) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }

}
